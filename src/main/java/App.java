import books.Book;
import books.BookManager;
import books.BookManagerService;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

public class App {

    public static void main(String... args) {
        Book book = new Book();
        book.setAuthor("Bartosz Kopciuch");
        book.setId("bk1");
        book.setIsbn("723-503-639");
        book.setPages(512);
        book.setPublisher("WI ZUT");
        book.setTitle("Zaliczonko na 5");
        book.setYear(2019);

        BookManager bm = new BookManagerService().getBookManagerPort();
        bm.addBook(book);

        List<Book> books = bm.searchByAuthor("Kopciuch");
        books.forEach(b -> System.out.println(ReflectionToStringBuilder.toString(b)));
    }
}
